package com.feldman.devtestforimmedia.controller;

import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by feldman on 15/10/2018.
 */

public class API {

    public static final String BASE_URL = "https://api.foursquare.com/v2/";

    static CustomInterceptor interceptor = new CustomInterceptor();
    static OkHttpClient okHttpClient = new OkHttpClient.Builder().addInterceptor(interceptor).build();

    static RxJava2CallAdapterFactory rxJava2CallAdapter = RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io());

    static Retrofit retrofit = new Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    public static Retrofit getRetrofit(){
        return retrofit;
    }

}
