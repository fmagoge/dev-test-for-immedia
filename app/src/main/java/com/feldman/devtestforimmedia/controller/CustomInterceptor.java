package com.feldman.devtestforimmedia.controller;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by feldman on 15/10/2018.
 */

public class CustomInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();
        HttpUrl originalHttpUrl = original.url();
        HttpUrl url =  originalHttpUrl.newBuilder()
                .addQueryParameter("insecure","cool")
                .addQueryParameter("client_id","CWHIXWZL3FV4YSG0ICDHJM4YPTU1VDHTNUFFTJJAGR1WVB2L")
                .addQueryParameter("client_secret","AUMWADBNJNGP5QOXVWR25CXNY42L0OM5HNEILMXZLIBDNTCT")
                .addQueryParameter("v","20171020")
                .build();
        Request.Builder requestBuilder = original.newBuilder()
                .url(url);
        Request request = requestBuilder.build();
        return chain.proceed(request);
    }
}
