package com.feldman.devtestforimmedia.controller;

import com.feldman.devtestforimmedia.models.BaseM;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by feldman on 15/10/2018.
 */

public interface FourSqaureAPI {
    @GET("venues/serch")
    Observable<BaseM> venueSearch(@Query("ll") String latlon);
}
