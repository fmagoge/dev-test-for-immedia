package com.feldman.devtestforimmedia.models;

/**
 * Created by feldman on 15/10/2018.
 */

public class BaseM {

    private Response response;

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }
}
