package com.feldman.devtestforimmedia.models;

/**
 * Created by feldman on 15/10/2018.
 */

public class Location {
    private String address;
    private int distance;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }
}
