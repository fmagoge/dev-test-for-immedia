package com.feldman.devtestforimmedia.models;

import java.util.List;

/**
 * Created by feldman on 15/10/2018.
 */

public class Response {
    private List<Venue> venues;

    public List<Venue> getVenues() {
        return venues;
    }

    public void setVenues(List<Venue> venues) {
        this.venues = venues;
    }
}
